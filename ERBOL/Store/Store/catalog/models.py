from django.core.exceptions import ValidationError
from django.db import models
from smart_selects.db_fields import ChainedForeignKey


class Currency(models.Model):
    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'

    title = models.CharField('Курс', max_length=255)

    def __str__(self):
        return self.title


class Catalog(models.Model):
    class Meta:
        verbose_name = 'Каталог'
        verbose_name_plural = 'Каталогы'

    name = models.CharField('Имя каталога', max_length=255)

    def __str__(self):
        return self.name


class SubCatalog(models.Model):
    class Meta:
        verbose_name = 'Под каталог'
        verbose_name_plural = 'Под каталогы'

    name = models.CharField('Имя под-каталога', max_length=255)
    catalog = models.ForeignKey(Catalog, models.SET_NULL, null=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    catalog = models.ForeignKey(verbose_name='Каталог', to=Catalog, on_delete=models.SET_NULL, null=True)

    sub_catalog = ChainedForeignKey(
        SubCatalog,
        verbose_name='Под-каталог',
        chained_field='catalog',
        chained_model_field='catalog',
        show_all=False,
        auto_choose=True,
        sort=True
    )

    name = models.CharField('Имя товара', max_length=255)
    price = models.IntegerField('Цена товара')
    is_currency = models.ForeignKey(Currency, models.SET_NULL, null=True)
    article = models.CharField(max_length=24, unique=True, blank=True, null=True)

    def save(self):
        if self.name == 'Тамеки' or self.name == 'тамеки':
            raise ValidationError("Мы не продаем табачные изделья")

    def __str__(self):
        return self.name


class Costumer(models.Model):
    class Meta:
        verbose_name = 'Покупатель'
        verbose_name_plural = 'Покупатели'

    name = models.CharField('Имя', max_length=255)
    phone_number = models.TextField('Телефон номер')
    location = models.CharField('Адресс доставки:', max_length=255)

    def clean(self):
        s = self.phone_number
        check = '+013468'

        if len(s) < 9 and len(s) > 13:
            raise ValidationError('Non Kyrgyzstan number!!!')
        else:
            if len(s) == 9:
                if s[0] in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            elif len(s) == 10:
                if s[0] != '0' or s[1] in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            elif len(s) == 12:
                if s[:3] != '996' or s[3] in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            elif len(s) == 13:
                if s[:4] != '+996' or s[4] in check:
                    raise ValidationError('Non Kyrgyzstan number!!!')
            else:
                raise ValidationError('Non Kyrgyzstan number!!!')

    def __str__(self):
        return f'{self.name}'


class Order(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    costumer_name = models.ForeignKey(verbose_name='Имя покупателья', to=Costumer, on_delete=models.SET_NULL, null=True)
    PAY_TYPE = {
        ('cash', 'Наличными'),
        ('card', 'Карта'),
        ('e-card', 'Электронный кошалалёк')
    }
    pay_type = models.CharField(verbose_name='Способ оплаты?', max_length=40, choices=PAY_TYPE, null=True)
    catalog = models.ForeignKey(verbose_name='Каталог', to=Catalog, on_delete=models.SET_NULL, null=True)

    sub_catalog = ChainedForeignKey(
        SubCatalog,
        verbose_name='Под-каталог',
        chained_field='catalog',
        chained_model_field='catalog',
        show_all=False,
        auto_choose=True,
        sort=True
    )
    quality = models.IntegerField('Количество')
    total_price = models.IntegerField('Общая сумма')

    def __str__(self):
        return f'{self.costumer_name}'


class ItemOrder(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    order_article = models.CharField(verbose_name='Артикул заказа ', max_length=255)
    product = models.ForeignKey(verbose_name='Продукт', to=Product, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(to=Order, on_delete=models.SET_NULL, null=True)
    quality = models.IntegerField()

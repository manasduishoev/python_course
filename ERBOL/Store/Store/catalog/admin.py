from django.contrib import admin
from catalog.models import *
from django.db.models import QuerySet


class OrderInline(admin.TabularInline):
    model = ItemOrder
    extra = 0
    can_delete = False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderInline]


class CustomerAdmin(admin.ModelAdmin):
    # чтобы не было видно
    # exclude = ['location']
    # только это поле будет виден
    # fields = ['name']
    # в главном будет отобрадаться эти поле
    list_display = ['name', 'phone_number']
    list_filter = ['name']
    search_fields = ['name']


@admin.action(description='Изменить каталог выбранных элементов')
def change_catalog(self, request, qs: QuerySet):
    qs.update(catalog="")

@admin.action(description='Дублировать товар')
def dublicate(self, request, qs: QuerySet):
    n = qs[0]
    n.article = None
    n.id = None
    n.save()

class CatalogAdmin(admin.ModelAdmin):
    # list_display = ['name']
    # list_editable = ['name']
    list_filter = ['name']


class SubCatalogAdmin(admin.ModelAdmin):
    class Meta:
        ordering = ['-catalog']

    list_display = ['name', 'catalog']
    list_filter = ['catalog']
    actions = [change_catalog]


class ProductAdmin(admin.ModelAdmin):
    list_display_links = ['price', 'name']
    list_display = ['catalog', 'name', 'price']
    search_fields = ['name']
    list_filter = ['catalog', 'sub_catalog']
    actions = [dublicate]


admin.site.register(Catalog, CatalogAdmin)
admin.site.register(SubCatalog, SubCatalogAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Currency)
admin.site.register(Costumer, CustomerAdmin)
admin.site.register(Order, OrderAdmin)
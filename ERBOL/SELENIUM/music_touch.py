from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

browser = webdriver.Firefox('./SAPAROV/SELENIUM/geckodriver')

browser.get("https://kmuzon.com")

search = browser.find_element(By.NAME, "story")
search.send_keys("Мирбек Атабеков")
search.send_keys(Keys.RETURN)

try:
    element = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.LINK_TEXT, "2"))
    )
    element.click()

    element2 = browser.find_element(By.XPATH, "/html/body/div[1]/div/div/main/div[2]/div[1]/div[6]/div/span")
    element2.click()
except:
    browser.quit()
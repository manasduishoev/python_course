document.querySelector('#contact-form').addEventListener('submit', (e) => {
    e.preventDefault();
    e.target.elements.name.value = '';
    e.target.elements.email.value = '';
    e.target.elements.message.value = '';
  });
let form = document.getElementById('form');
let button = document.getElementById('button');
button.addEventListener('click', () => form.reset());
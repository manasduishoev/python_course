# Generated by Django 3.2 on 2022-06-11 10:09

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0010_alter_news_created_add'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='created_add',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 11, 16, 9, 28, 219705)),
        ),
    ]

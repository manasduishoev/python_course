from datetime import datetime

from django.db import models



class News(models.Model):
    name = models.CharField(max_length=255)
    text = models.TextField()
    created_date = models.DateTimeField(default=datetime.now())
    is_active = models.BooleanField(default=False)
    count_view = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to='image', null=True)
    file = models.FileField(upload_to='image', null=True)

    def __str__(self):
        return self.name


class Comment(models.Model):
    news = models.ForeignKey(to=News, on_delete=models.CASCADE)
    title = models.TextField()
    created_date = models.DateTimeField(default=datetime.now())

    def __str__(self):
        return self.title



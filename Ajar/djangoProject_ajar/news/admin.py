from django.contrib import admin

from news.models import News, Comment

class CommentInnline(admin.TabularInline):
    model = Comment
    extra = 1
    #can_delete = False

class NewsAdmin(admin.ModelAdmin):
    inlines = [CommentInnline]
    list_display = ['name', 'created_date']
    list_display_links = ['name']
    #list_editable = ['']
    search_fields = ['name']
    exclude = ['file']
    #fields = (('name','created_date'),'text')
    #fields = ['name', 'created_date']
    fieldsets = (
        ('Контент',{
            'fields':('name','text')
        }),
        ('Files',{
            'fields':('created_date','count_view'),
        }),
                )
    ordering = ['name']
    list_per_page = 2

class CommentAdmin(admin.ModelAdmin):
    model=Comment
    list_display = ['title','get_news_name']
    list_filter = ['news']

    def get_news_name (self,obj):
        return obj.news.name

admin.site.register(News, NewsAdmin)
admin.site.register(Comment,CommentAdmin)


from django.contrib import admin

# Register your models here.
class NewsAdmin(admin.ModelAdmin):
    list_display = ['name','text','created_add','boolen','count']
    list_display_links = ['text']
    list_editable = ['name']
    search_fields = ['name']

from news.models import News
admin.site.register(News,NewsAdmin)


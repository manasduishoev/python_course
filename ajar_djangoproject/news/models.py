from datetime import datetime

from django.db import models

class News(models.Model):
    name=models.CharField(max_length=255)
    text=models.TextField()
    created_add=models.DateTimeField(default=datetime.now())
    boolen = models.BooleanField(null=True)
    count = models.PositiveIntegerField(default=0)
    #img = models.ImageField(upload_to='img')

    def __str__(self):
        return self.name


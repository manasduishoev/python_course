from django.db import models

# Create your models here.
class News(models.Model):
    name = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(null=True)
    count_view = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to='img', null=True, blank=True)
    file = models.FileField(upload_to='img', null=True)

    def __str__(self):
        return self.name

class Comment(models.Model):
    title = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

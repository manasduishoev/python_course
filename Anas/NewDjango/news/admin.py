from django.contrib import admin

from news.models import News, Comment

class NewsAdmin(admin.ModelAdmin):
    list_display = ['name', 'text', 'created_at']
    list_display_links = ['text']
    list_editable = ['name']
    search_fields = ['name', 'text']

admin.site.register(News, NewsAdmin)
admin.site.register(Comment)

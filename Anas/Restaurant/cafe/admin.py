from django.contrib import admin
from django.db.models import QuerySet

from cafe.models import *
from client.models import *

class ItemOrderAdmin(admin.TabularInline):
    model = ItemOrders
    extra = 0

class OrderAdmin(admin.ModelAdmin):
    inlines = [ItemOrderAdmin]

class FoodListAdmin(admin.ModelAdmin):
    model = Food
    list_display = ['name']
    list_filter = ['menu']

admin.site.register(Order, OrderAdmin)
admin.site.register(City)
admin.site.register(Restaurant)
admin.site.register(Menu)
admin.site.register(Food, FoodListAdmin)

admin.site.index_title = "cafe"
admin.site.site_header = "CAFE"
admin.site.site_title = "ANAS"
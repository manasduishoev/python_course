from django.contrib import admin
from django.db.models import QuerySet
from django.contrib import messages

from client.models import Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'surname', 'phone_number', 'age', 'address']
    list_display_links = ['name', 'surname']
    list_per_page = 5
    search_fields = ['name', 'surname', 'address']
    list_editable = ['age', 'address']
    actions = ['change_name']

    @admin.action(description='Изменить возраст')
    def change_name(self, request, qs: QuerySet):
        qs.update(age=20)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if Client.phone_number[:3] != '996':
            raise Exception("Ката! Бул Кыргызстандын номери эмес!")
        return super(ClientAdmin, self).save(self, force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

admin.site.register(Client, ClientAdmin)
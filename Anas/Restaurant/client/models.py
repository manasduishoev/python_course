from django.db import models

class Client(models.Model):
    name = models.CharField(max_length=50, verbose_name='Имя')
    surname = models.CharField(max_length=50, verbose_name='Фамилия')
    age = models.PositiveIntegerField(verbose_name='Возраст', default=0)
    address = models.CharField(verbose_name='Адрес', max_length=100)
    phone_number = models.TextField(verbose_name='Телефон номер', max_length=15)

    def __str__(self):
        return '{surname} {name}'.format(surname=self.surname,name=self.name)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        unique_together = ['name', 'surname']
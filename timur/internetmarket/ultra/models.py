from django.db import models
from smart_selects.db_fields import ChainedForeignKey


class Katalog(models.Model):
    class Meta:
        verbose_name = ("Каталог")
        verbose_name_plural = ("Каталоги")

    title = models.CharField("Список каталогов", max_length=50)

    def __str__(self):
        return self.title


class SubKatalog(models.Model):
    class Meta:
        verbose_name = ("Под каталог")
        verbose_name_plural = ("Под каталоги")

    catalog = models.ForeignKey(to=Katalog, on_delete=models.CASCADE)
    title = models.CharField("Под каталог", max_length=100)

    def __str__(self):
        return self.title

class Tovar(models.Model):
    class Meta:
        verbose_name = ("Товар")
        verbose_name_plural = ("Товары")

    katalog = models.ForeignKey(Katalog, on_delete=models.SET_NULL,null=True, blank=True, verbose_name="Выбрать каталог")
    subcatalog = models.ForeignKey(
        SubKatalog,
        # chained_field="category",
        # chained_model_field="category",
        # show_all=False,
        # auto_choose=True,
        # sort=True,
        on_delete=models.SET_NULL,
        null=True, blank=True,
        verbose_name="Выбрать под каталог")
    name = models.CharField("Название товара", max_length=50)
    price = models.IntegerField(verbose_name="Цена товара")


    def __str__(self):
        return self.name


class Client(models.Model):
    class Meta:
        verbose_name = ("Клиент")
        verbose_name_plural = ("Клиенты")

    name = models.CharField("Имя клиента", max_length=50)
    tel = models.CharField("Номер телефона", max_length=50)
    login = models.CharField("Логин", max_length=30)
    password = models.CharField("Пароль", max_length=20)
    live_place = models.CharField("Место жительствa", max_length=255)

    def __str__(self):
        return self.name

class Basket(models.Model):
    class Meta:
        verbose_name = ("Моя корзинка")
        verbose_name_plural = ("Мои корзинки")

    playment_method = (
        ("cash", "наличка"),
        ("card", "карта"),
        ("mob_cash", "мобильный кошолек"),

    )


    client_name = models.ForeignKey(verbose_name="Имя клиента", to = Client, on_delete=models.CASCADE)
    playment = models.CharField(verbose_name="Способ оплаты", max_length=20, choices=playment_method, default=True)
    location = models.CharField(verbose_name="Место доставки", max_length=255)

    def __str__(self):
        return self.Meta

class ItemTovar(models.Model):
    product = models.ForeignKey(Tovar, models.SET_NULL, null=True, blank=True)
    order = models.ForeignKey(Basket, models.SET_NULL, null=True, blank=True)
    quantity = models.IntegerField()



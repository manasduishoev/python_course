from django.http import HttpResponse, HttpRequest, HttpResponseNotFound, Http404
from django.shortcuts import render, redirect

menu = ["Главная страница", 'О нас', 'Котегории', "контакты"]


def index(request):
    return render(request, 'news/index.htm', {'menu': menu, 'title': 'Главная страница'})


def about(request):
    return render(request, 'news/about.html', {'menu': menu, 'title': 'О нас'})


def categories(request, year):
    if int(year) > 2022 or int(year) < 2010:
        return redirect('home', permanent=True)

    return HttpResponse(
        f'<h1>{year}s category</h1>')  # if int(year) < 2023 else HttpResponse('<h1>have not that date</h1>')


def pageNotFound(request, exeption):
    return HttpResponseNotFound("<h1>к сожелению страница не найдена</h1>")

from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('', index, name ='home'), # http://127.0.0.1:8000/
    path('about', about, name = 'about'),
    re_path(r'^categories/(?P<year>[0-9]{4})/', categories)# http://127.0.0.1:8000/categories/2020/
]

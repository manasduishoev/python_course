from django.contrib import admin

from glovo.models import *
class ItemOrderAdmin(admin.TabularInline):
    model = ItemOrder
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False
    def has_change_permission(self, request, obj=None):
        return False


class OrderAdmin(admin.ModelAdmin):
    inlines = [ItemOrderAdmin]

    def has_delete_permission(self, request, obj=None):
        return False


    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(Product)
admin.site.register(Establishment)
admin.site.register(Category)
admin.site.register(Order, OrderAdmin)

from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=255)


class SubCategory(models.Model):
    category = models.ForeignKey(Category, models.CASCADE)
    title = models.CharField(max_length=255)


class Currency(models.Model):
    title = models.CharField(max_length=255)


class Product(models.Model):
    title = models.CharField(max_length=255)
    sub_category = models.ForeignKey(SubCategory, models.SET_NULL, null=True, blank=True)
    article_number = models.IntegerField()
    price = models.IntegerField()
    currency = models.ForeignKey(Currency, models.SET_NULL, null=True, blank=True)
    photo = models.ImageField(upload_to='product/')


class Order(models.Model):
    name = models.CharField(max_length=100)
    is_delivery = models.BooleanField(default=True)
    is_cash = models.BooleanField()
    total_price = models.IntegerField()
    address = models.TextField()
    total_item = models.IntegerField()


class ItemOrder(models.Model):
    product = models.ForeignKey(Product, models.SET_NULL, null=True, blank=True)
    product_name = models.CharField(max_length=255)
    order = models.ForeignKey(Order, models.SET_NULL, null=True, blank=True)
    quantity = models.IntegerField()

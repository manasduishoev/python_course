from django.contrib import admin

# Register your models here.
from main.models import News, Comment


class NewsAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_date']
    list_display_links = ['created_date']
# list_display_links = list_display
    list_editable = ['name']
    search_fields = ['name']
#     бул кайсыны озгортосо болот ошону кылат

admin.site.register(News, NewsAdmin)
admin.site.register(Comment)

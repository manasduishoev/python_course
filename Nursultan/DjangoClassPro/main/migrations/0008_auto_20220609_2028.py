# Generated by Django 3.2.13 on 2022-06-09 14:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20220609_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='file',
            field=models.FileField(null=True, upload_to='image'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 9, 20, 28, 52, 960340)),
        ),
        migrations.AlterField(
            model_name='news',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 9, 20, 28, 52, 959549)),
        ),
    ]

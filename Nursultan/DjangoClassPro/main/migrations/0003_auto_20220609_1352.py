# Generated by Django 3.2.13 on 2022-06-09 13:52

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20220609_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 9, 13, 52, 16, 804461)),
        ),
        migrations.AlterField(
            model_name='news',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 9, 13, 52, 16, 803936)),
        ),
    ]
